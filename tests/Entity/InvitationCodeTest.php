<?php
namespace App\Tests\Entity;

use App\Entity\InvitationCode;
use DateTime;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InvitationCodeTest extends WebTestCase{

  use FixturesTrait;
    public function getEntity():InvitationCode
    {
       return (new InvitationCode())
       ->setCode('12345')
       ->setDescription('description de test')
       ->setExpireAt(new DateTime());
    }

    public function assertAsErrors(InvitationCode $code, int $number=0)
    {
        self::bootKernel();
        $errors= self::$container->get('validator')->validate($code);
        $messages=[];
        /** @var ConstraintViolation $error */
        foreach($errors as $error){
            $messages[]=$error->getPropertyPath().'=>'. $error->getMessage();
        }
        $this->assertCount($number,$errors, implode(', ',$messages));
    }

    public function testValidEntity()
    {
        $this->assertAsErrors($this->getEntity(),0); 
    }

    public function testInvalidEntity()
    {
        $this->assertAsErrors($this->getEntity()->setCode('abc'),1);
        $this->assertAsErrors($this->getEntity()->setCode('33'),1);
    }
    public function testInvalidBlankCodeEntity()
    {
        $this->assertAsErrors($this->getEntity()->setCode(''),1);
    }

    public function testInvalidDescriptionEntity()
    {
        $this->assertAsErrors($this->getEntity()->setDescription(''),1);

    }

    public function testInvalidUsedCodeEntity()
    {   
        $this->loadFixtureFiles([
            dirname(__DIR__).'\Fixtures\InvalidCodeTestFixtures.yaml'
        ]);
        $this->assertAsErrors($this->getEntity()->setCode('11111'),1);
    }
}