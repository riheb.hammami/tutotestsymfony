<?php

namespace App\Tests\Repository;

use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRepositoryTest extends WebTestCase{

    use FixturesTrait;
  
    public function testCount(){
        self::bootKernel();
        //$this->loadFixtures([UserFixtures::class]);
        $users=$this->loadFixtureFiles([
            __DIR__.'/UserRepositoryTestFixtures.yaml'
        ]);
        //$users[user1];
        $users= self::$container->get(UserRepository::class)->count([]);
        $this->assertEquals(10,$users);
    }
}

